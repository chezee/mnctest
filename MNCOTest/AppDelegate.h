//
//  AppDelegate.h
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

