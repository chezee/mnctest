//
//  MNCOObject.m
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MNCOObject.h"
#import "Backendless.h"

@implementation Product

+ (NSMutableArray *) getObjectOnPage:(NSInteger)page {
    NSMutableArray *pageArray;
    id<IDataStore> dataStore = [[[Backendless sharedInstance] persistenceService] of:[Product class]];
    BackendlessCollection *stuff = [dataStore find:nil];
    
    //Size of products on page is only 10 so it's assigning first 10 products or
    //last 10 products because of total count 12
    [stuff pageSize:10];
    pageArray = [NSMutableArray arrayWithArray:[[stuff getPage:page]getCurrentPage]];
    NSLog(@"%@", pageArray);
    return pageArray;
}

@end
