//
//  TableViewCell.h
//  MNCOTest
//
//  Created by Илья Пупкин on 1/29/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNCOObject.h"

@interface MNCOTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImage *image;

- (void) configureWithItem:(Product *)productForCell andSize:(CGSize)size;

@end
