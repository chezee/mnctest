//
//  MNCOObject.h
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (strong, nonatomic) NSString *objectID;
@property (strong, nonatomic) NSDate *created;
@property (strong, nonatomic) NSDate *updated;
@property (strong, nonatomic) NSString *productName;
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSString *imageURL;
@property double price;

+ (NSMutableArray *) getObjectOnPage:(NSInteger)page;

@end
