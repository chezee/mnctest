//
//  MNCODetailViewController.m
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MNCODetailViewController.h"

@interface MNCODetailViewController ()

@end

@implementation MNCODetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setting up imageView
    [self.imageView setFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 30, self.view.bounds.size.width / 2, 120)];
    self.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.product imageURL]]]];
    
    //Setting up label with name of product
    [self.name setFrame:CGRectMake(self.view.bounds.size.width / 2 + 12, self.navigationController.navigationBar.frame.size.height + 30, self.view.bounds.size.width - (self.view.bounds.size.width / 2 + 16), 20)];
    self.name.text = [_product productName];
    self.name.adjustsFontSizeToFitWidth = YES;
    
    //Setting up price label
    [self.price setFrame:CGRectMake(self.view.bounds.size.width / 2 + 12, self.navigationController.navigationBar.frame.size.height + 58, self.view.bounds.size.width - (self.view.bounds.size.width / 2 + 16), 20)];
    NSNumber *priceNumber = [NSNumber numberWithDouble:self.product.price];
    self.price.text = [NSString stringWithFormat:@"%@ $", [priceNumber stringValue]];
    
    //Setting up description of product in TextView
    [self.descr setFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 148, self.view.bounds.size.width, self.view.bounds.size.height - (self.imageView.bounds.size.height + 8))];
    self.descr.text = [_product descr];

}

@end
