//
//  main.m
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
