//
//  MNCODetailViewController.h
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNCOObject.h"

@interface MNCODetailViewController : UIViewController

@property (strong ,nonatomic) Product *product;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UITextView *descr;
@property (strong, nonatomic) IBOutlet UILabel *price; 

@end
