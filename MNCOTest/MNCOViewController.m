//
//  ViewController.m
//  MNCOTest
//
//  Created by Илья Пупкин on 1/28/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MNCOViewController.h"
#import "MNCOObject.h"
#import "MNCOTableViewCell.h"
#import "MNCODetailViewController.h"
static NSString *cellIdentifier = @"cellIdentifier";

@interface MNCOViewController ()

@end

@implementation MNCOViewController {
    NSMutableArray *productsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.table registerClass:[MNCOTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    productsArray = [Product getObjectOnPage:0];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [productsArray count] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *postCellId = @"postCell";
    static NSString *moreCellId = @"moreCell";
    MNCOTableViewCell *cell = nil;
    
    NSUInteger row = [indexPath row];
    NSUInteger count = [productsArray count];
    
    //Because of indexPath.row 0..9 for example and count of products in array starts from 1 we can do this
    if (row == count) {
        cell = [[MNCOTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellId];
        cell.textLabel.text = @"Load more";
        cell.textLabel.textColor = [UIColor blueColor];

    } else {
        cell = [[MNCOTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:postCellId];
        [cell configureWithItem:[productsArray objectAtIndex:indexPath.row] andSize:self.view.bounds.size];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //If cell out of count of products it's download more products and adds to array and reload table
    //If it has product going to detailed view
    if (indexPath.row + 1 > [productsArray count]) {
        [productsArray addObjectsFromArray:[NSArray arrayWithArray:[Product getObjectOnPage:1]]];
        for (int i = 0; i < [productsArray count]; i++) {
            Product *itemOne = [productsArray objectAtIndex:i];
            for (int a=i+1; a < [productsArray count]; a++) {
                Product *itemTwo = [productsArray objectAtIndex:a];
                if([[itemOne productName] isEqualToString:[itemTwo productName]] || itemOne.price == itemTwo.price){
                    [productsArray removeObject:itemTwo];
                }
            }
        }
        [self.table reloadData];
    } else {
    MNCODetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    detailViewController.product = [productsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

@end
