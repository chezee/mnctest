//
//  TableViewCell.m
//  MNCOTest
//
//  Created by Илья Пупкин on 1/29/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MNCOTableViewCell.h"


@implementation MNCOTableViewCell

- (void)configureWithItem:(Product *)productForCell andSize:(CGSize)size {
    //Making product name label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(size.width/3 + 16, 8, size.width - (size.width / 3 - 12), 20)];
    label.adjustsFontSizeToFitWidth = YES;
    label.text = [productForCell productName];
    [self addSubview:label];
    
    //Making description label
    UITextView *detailedLabel = [[UITextView alloc] initWithFrame:CGRectMake(size.width / 3 + 16, 28, size.width - (size.width / 3 + 8), 50)];
    detailedLabel.scrollEnabled = NO;
    detailedLabel.userInteractionEnabled = NO;
    detailedLabel.text = [productForCell descr];
    [detailedLabel sizeThatFits:CGSizeMake(size.width - (size.width / 3 + 16), 50)];
    [self addSubview:detailedLabel];
    
    //TODO:Remake with multithreading to prevent UI freezing
    CGSize imageSize = {size.width / 3, 100};
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        _image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[productForCell imageURL]]]];
        _image = [self imageWithImage:_image scaledToSize:imageSize];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[self imageView] setImage:_image];
            [self setNeedsLayout];
        });
    });
}

//Resizing image for cell
- (UIImage*)imageWithImage:(UIImage*)someImage scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [someImage drawInRect:CGRectMake(0,4,newSize.width,newSize.height-4)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
